import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IMqttMessage, MqttService } from 'ngx-mqtt';
import { Subscription } from 'rxjs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private subscription: Subscription;
  messages
  constructor( private _mqttService: MqttService , public navCtrl: NavController) {

    this._mqttService.observe("test").subscribe((message: IMqttMessage) => {
      this.messages = message.payload.toString();

      console.log(this.messages)
    })


  }

}
